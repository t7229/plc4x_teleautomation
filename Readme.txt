1. Initialisiserung
	a. Anschließen der SPS über Ethernetkabel an den Computer
	b. Mobilen Hotspot einrichten (Addressraum 192.168.137.1/24)
	c. RaspberryPi mit der statischen IP 168.196.137.225
2. Beschreiben der SPS mit dem Programm
	a. Ecockpit starten
	b. Projekt laden
	c. Netzwerkscan
	d. Auswahl SPS mit Modulen
	e. Verbindung aufbauen
	f. mit Download einloggen (Überspielen des aktuellen Projektes)
3. Virtual Modbus starten
	a. ModbusPal 1.6b starten
	b. Virtuellen Modbus anlegen
	c. Überprüfe, dass der Port auf 502 festgelegt ist
	d. Start des Programmes über Run
4. Über Putty auf RaspberryPi einloggen
	a. Grafana Starten über folgende Befehle
	b. Zugriff auf Grafana über folgenden Port 3000
5. Triggerung Fußgängerampel und Powersignal
	a. Über den virtuellen Modbus
	b. Klick auf Symbol Auge
	c. Doppelklick auf abzuändernden Value
	d. 0 bzw. 1 eingeben
	e. mit Enter bestätigen