package iat;

import java.util.concurrent.ExecutionException;

import org.apache.plc4x.java.api.messages.PlcReadResponse;
import org.apache.plc4x.java.api.messages.PlcSubscriptionRequest;
import org.apache.plc4x.java.api.messages.PlcSubscriptionResponse;
import org.apache.plc4x.java.api.model.PlcSubscriptionHandle;
import org.apache.plc4x.java.api.types.PlcResponseCode;

public class OPCUAHandler extends PLC4XHandler{
	public PlcSubscriptionResponse sResponse;
	
	// Operations: read / write / subscribe
	
	public OPCUAHandler(String connectionString) {
		super(connectionString);
		
		
	}

	// Input: identifierString
	// Output: Integer value of requested ariable
	public int subscribe(String identifierString) {
		this.sResponse = subscribeVariable(identifierString);
		System.out.println("Reading finished");
		String answer = printsResponse(this.sResponse);
		if ((answer == "false")) {
			return 0;
		} else if (answer == "true") {
			return 1;
		}else {
			return Integer.parseInt(answer);	
		}

	}
	
	public PlcSubscriptionResponse subscribeVariable(String identifier) {
		if (this.Connection.getMetadata().canSubscribe()) {
        	PlcSubscriptionRequest.Builder sBuilder = this.Connection.subscriptionRequestBuilder();
        	sBuilder.addChangeOfStateField("value", identifier);
        	
        	PlcSubscriptionRequest sRequest = sBuilder.build();
        	
        	PlcSubscriptionResponse sResponse;
			try {
				sResponse = sRequest.execute().get();
				return sResponse;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
        	
        }
		return null;
	}
	

	public String printsResponse(PlcSubscriptionResponse response) {
		String result = "";
		for (String fieldName : response.getFieldNames()) {
			if (response.getResponseCode(fieldName) == PlcResponseCode.OK) {

				int numValues = ((PlcReadResponse) response).getNumberOfValues(fieldName);
				// If it's just one element, output just one single line.
				if (numValues == 1) {
					logger.info("Value[" + fieldName + "]: " + ((PlcReadResponse) response).getObject(fieldName));
					result = String.valueOf(((PlcReadResponse) response).getObject(fieldName));

				}
				// If it's more than one element, output each in a single row.
				else {
					logger.info("Value[" + fieldName + "]:");
					for (int i = 0; i < numValues; i++) {
						logger.info(" - " + ((PlcReadResponse) response).getObject(fieldName, i));
					}
				}
			}
			// Something went wrong, to output an error message instead.
			else {
				logger.error("Error[" + fieldName + "]: " + response.getResponseCode(fieldName).name());
			}
		}
		return result;

	}
}
