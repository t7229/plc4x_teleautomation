package iat;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import iat.MainClass;

public class MainClass {

	// ModBus Variablen
	public static final String IPADDRESS_MODBUS = "modbus://127.0.0.1:502";
	public static final String IDENTIFIER_PowerButton_MB = "holding-register:1";
	public static final String IDENTIFIER_AmpelTaster_MB = "holding-register:2";

	// OPCUA Variablen
	public static final String IPADDRESS_OPCUA = "opcua://169.254.151.240:4840?discovery=true&username=admin&password=wago";
	public static final String IDENTIFIER_PowerButton_UA = "ns=4;s=|var|WAGO 750-8212 PFC200 G2 2ETH RS.Application.GVL.TL_Power;BOOL";
	public static final String IDENTIFIER_AmpelTaster_UA = "ns=4;s=|var|WAGO 750-8212 PFC200 G2 2ETH RS.Application.GVL.TL_Switch_request;BOOL";

	public static final String IDENTIFIER_FG_back_UA = "ns=4;s=|var|WAGO 750-8212 PFC200 G2 2ETH RS.Application.GVL.FG_back;BOOL";
	public static final String IDENTIFIER_FG_green_UA = "ns=4;s=|var|WAGO 750-8212 PFC200 G2 2ETH RS.Application.GVL.FG_green;BOOL";
	public static final String IDENTIFIER_FG_red_UA = "ns=4;s=|var|WAGO 750-8212 PFC200 G2 2ETH RS.Application.GVL.FG_red;BOOL";
	public static final String IDENTIFIER_KFZ_green_UA = "ns=4;s=|var|WAGO 750-8212 PFC200 G2 2ETH RS.Application.GVL.KFZ_green;BOOL";
	public static final String IDENTIFIER_KFZ_red_UA = "ns=4;s=|var|WAGO 750-8212 PFC200 G2 2ETH RS.Application.GVL.KFZ_red;BOOL";
	public static final String IDENTIFIER_FG_yellow_UA = "ns=4;s=|var|WAGO 750-8212 PFC200 G2 2ETH RS.Application.GVL.KFZ_yellow;BOOL";

	// MySQL Variablen
	public static final String url = "jdbc:mysql://192.168.137.225:3306/ampelanlage?useSSL=false";
	public static final String user = "test";
	public static final String password = "123";

	String query = "SELECT VERSION()";

	// INIT ModBusHandler
	public static ModBusHandler modbushandler = new ModBusHandler(IPADDRESS_MODBUS);
	// INIT OPCUAHandler
	public static OPCUAHandler opcuahandler = new OPCUAHandler(IPADDRESS_OPCUA);

	public static void main(String[] args) {

		
		Thread t1 =new Thread() {
			public void run() {
				filldatabank();	
			}	
		};
		
		Thread t2 =new Thread() {
			public void run() {
				translator();	
			}	
		};
		t1.start();
		t2.start();
	
		// System.out.println("Programm finished");

	}

	
	public static void translator() {

		// Init Power & AmpelTaster variables
		int answer_PowerButton = modbushandler.read(IDENTIFIER_PowerButton_MB);
		int answer_AmpelTaster = modbushandler.read(IDENTIFIER_AmpelTaster_MB);

		int answer_PowerButton_old = Integer.valueOf(answer_PowerButton);
		int answer_AmpelTaster_old = Integer.valueOf(answer_AmpelTaster);

		// Set Power state of the PLC
		if (answer_PowerButton == 1) {
			opcuahandler.write(IDENTIFIER_PowerButton_UA, true);
		} else if (answer_PowerButton == 0) {
			opcuahandler.write(IDENTIFIER_PowerButton_UA, false);
		}

		// Loop for value change
		while (true) {

			// Reading ModBus
			answer_PowerButton = modbushandler.read(IDENTIFIER_PowerButton_MB);
			answer_AmpelTaster = modbushandler.read(IDENTIFIER_AmpelTaster_MB);

			if ((answer_PowerButton != answer_PowerButton_old) || (answer_AmpelTaster != answer_AmpelTaster_old)) {

				// Write Database

				try (Connection con = DriverManager.getConnection(url, user, password);
						Statement st = con.createStatement()) {
					String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS").format(new java.util.Date());
					String sql = "INSERT INTO Ampel_Taster(Timestamp, Power, Request) VALUES(" + "'" + timeStamp + "',"
							+ "'" + answer_PowerButton + "'" + "," + "'" + answer_AmpelTaster + "'" + ")";

					// String sql = "DELETE FROM testing WHERE time < (NOW() - INTERVAL 13
					// MINUTE)";
					System.out.println(sql);
					st.executeUpdate(sql);

				} catch (SQLException ex) {
					System.out.println("Error:" + ex);
				}

				// Write OPCUA Variables
				if (answer_AmpelTaster == 1) {
					opcuahandler.write(IDENTIFIER_AmpelTaster_UA, true);
					try {
						Thread.sleep(500);
					} catch (InterruptedException ie) {
						Thread.currentThread().interrupt();
					}
					modbushandler.write(IDENTIFIER_AmpelTaster_MB, false);

				} else if (answer_AmpelTaster == 0) {
					opcuahandler.write(IDENTIFIER_AmpelTaster_UA, false);
				}

				if (answer_PowerButton == 1) {
					opcuahandler.write(IDENTIFIER_PowerButton_UA, true);
				} else if (answer_PowerButton == 0) {
					opcuahandler.write(IDENTIFIER_PowerButton_UA, false);
				}
			} else {
				try (Connection con = DriverManager.getConnection(url, user, password);
						Statement st = con.createStatement()) {
					String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS").format(new java.util.Date());
					String sql = "INSERT INTO Ampel_Taster(Timestamp, Power, Request) VALUES(" + "'" + timeStamp + "',"
							+ "'" + answer_PowerButton + "'" + "," + "'" + answer_AmpelTaster + "'" + ")";

					// String sql = "DELETE FROM testing WHERE time < (NOW() - INTERVAL 13
					// MINUTE)";
					System.out.println(sql);
					st.executeUpdate(sql);

				} catch (SQLException ex) {
					System.out.println("Error:" + ex);
				}
			}

			answer_PowerButton_old = Integer.valueOf(answer_PowerButton);
			answer_AmpelTaster_old = Integer.valueOf(answer_AmpelTaster);

			try {
				Thread.sleep(1000);
			} catch (InterruptedException ie) {
				Thread.currentThread().interrupt();
			}
		}
	}

	// Connects to database on RaspberryPi
	public static void filldatabank() {
		// Init OPCUA variables
		
		int answer_FG_back = opcuahandler.read(IDENTIFIER_FG_back_UA);
		int answer_FG_green = opcuahandler.read(IDENTIFIER_FG_green_UA);
		int answer_FG_red = opcuahandler.read(IDENTIFIER_FG_red_UA);
		int answer_KFZ_green = opcuahandler.read(IDENTIFIER_KFZ_green_UA);
		int answer_KFZ_red =opcuahandler.read(IDENTIFIER_KFZ_red_UA);
		int answer_KFZ_yellow = opcuahandler.read(IDENTIFIER_FG_yellow_UA);
		
		
		int answer_FG_back_old = Integer.valueOf(answer_FG_back);
		int answer_FG_green_old = Integer.valueOf(answer_FG_green);
		int answer_FG_red_old = Integer.valueOf(answer_FG_red);
		int answer_KFZ_green_old = Integer.valueOf(answer_KFZ_green);
		int answer_KFZ_red_old = Integer.valueOf(answer_KFZ_red);
		int answer_KFZ_yellow_old = Integer.valueOf(answer_KFZ_yellow);

		try (Connection con = DriverManager.getConnection(url, user, password);
				Statement st = con.createStatement()) {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS").format(new java.util.Date());
			String sql = "INSERT INTO ampel_auto(Timestamp, Rot, Gruen, Gelb) VALUES(" 
					+ "'" + timeStamp + "',"
					+ "'" + answer_KFZ_red + "'" + "," 
					+ "'" + answer_KFZ_green + "'" + ","
					+ "'" + answer_KFZ_yellow + "'" 
					+ ")";
			// String sql = "DELETE FROM testing WHERE time < (NOW() - INTERVAL 13
			// MINUTE)";
			System.out.println(sql);
			st.executeUpdate(sql);

		} catch (SQLException ex) {
			System.out.println("Error:" + ex);
		}
		
		//
		try (Connection con = DriverManager.getConnection(url, user, password);
				Statement st = con.createStatement()) {
			String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS").format(new java.util.Date());
			String sql = "INSERT INTO ampel_fuss(Timestamp, Rot, Gruen, Back) VALUES(" 
					+ "'" + timeStamp + "',"
					+ "'" + answer_FG_red + "'" + "," 
					+ "'" + answer_FG_green + "'" + ","
					+ "'" + answer_FG_back + "'" 
					+ ")";
			// String sql = "DELETE FROM testing WHERE time < (NOW() - INTERVAL 13
			// MINUTE)";
			System.out.println(sql);
			st.executeUpdate(sql);

		} catch (SQLException ex) {
			System.out.println("Error:" + ex);
		}
		
		while(true) {
			answer_FG_back = opcuahandler.read(IDENTIFIER_FG_back_UA);
			answer_FG_green = opcuahandler.read(IDENTIFIER_FG_green_UA);
			answer_FG_red = opcuahandler.read(IDENTIFIER_FG_red_UA);
			answer_KFZ_green = opcuahandler.read(IDENTIFIER_KFZ_green_UA);
			answer_KFZ_red =opcuahandler.read(IDENTIFIER_KFZ_red_UA);
			answer_KFZ_yellow = opcuahandler.read(IDENTIFIER_FG_yellow_UA);
			
			if ((answer_FG_back!=answer_FG_back_old) || (answer_FG_green!=answer_FG_green_old) || (answer_FG_red!=answer_FG_red_old) || (answer_KFZ_green!=answer_KFZ_green_old) || (answer_KFZ_red != answer_KFZ_red_old)) {
				try (Connection con = DriverManager.getConnection(url, user, password);
						Statement st = con.createStatement()) {
					String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS").format(new java.util.Date());
					String sql = "INSERT INTO ampel_auto(Timestamp, Rot, Gruen, Gelb) VALUES(" 
							+ "'" + timeStamp + "',"
							+ "'" + answer_KFZ_red + "'" + "," 
							+ "'" + answer_KFZ_green + "'" + ","
							+ "'" + answer_KFZ_yellow + "'" 
							+ ")";
					// String sql = "DELETE FROM testing WHERE time < (NOW() - INTERVAL 13
					// MINUTE)";
					System.out.println(sql);
					st.executeUpdate(sql);

				} catch (SQLException ex) {
					System.out.println("Error:" + ex);
				}
				
				
				// Write to database
				try (Connection con = DriverManager.getConnection(url, user, password);
						Statement st = con.createStatement()) {
					String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS").format(new java.util.Date());
					String sql = "INSERT INTO ampel_fuss(Timestamp, Rot, Gruen, Back) VALUES(" 
							+ "'" + timeStamp + "',"
							+ "'" + answer_FG_red + "'" + "," 
							+ "'" + answer_FG_green + "'" + ","
							+ "'" + answer_FG_back + "'" 
							+ ")";
					// String sql = "DELETE FROM testing WHERE time < (NOW() - INTERVAL 13
					// MINUTE)";
					System.out.println(sql);
					st.executeUpdate(sql);

				} catch (SQLException ex) {
					System.out.println("Error:" + ex);
				}
			} else {

				try (Connection con = DriverManager.getConnection(url, user, password);
						Statement st = con.createStatement()) {
					String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS").format(new java.util.Date());
					String sql = "INSERT INTO ampel_auto(Timestamp, Rot, Gruen, Gelb) VALUES(" 
							+ "'" + timeStamp + "',"
							+ "'" + answer_KFZ_red + "'" + "," 
							+ "'" + answer_KFZ_green + "'" + ","
							+ "'" + answer_KFZ_yellow + "'" 
							+ ")";
					// String sql = "DELETE FROM testing WHERE time < (NOW() - INTERVAL 13
					// MINUTE)";
					System.out.println(sql);
					st.executeUpdate(sql);

				} catch (SQLException ex) {
					System.out.println("Error:" + ex);
				}
				
				//
				try (Connection con = DriverManager.getConnection(url, user, password);
						Statement st = con.createStatement()) {
					String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS").format(new java.util.Date());
					String sql = "INSERT INTO ampel_fuss(Timestamp, Rot, Gruen, Back) VALUES(" 
							+ "'" + timeStamp + "',"
							+ "'" + answer_FG_red + "'" + "," 
							+ "'" + answer_FG_green + "'" + ","
							+ "'" + answer_FG_back + "'" 
							+ ")";
					// String sql = "DELETE FROM testing WHERE time < (NOW() - INTERVAL 13
					// MINUTE)";
					System.out.println(sql);
					st.executeUpdate(sql);

				} catch (SQLException ex) {
					System.out.println("Error:" + ex);
				}
			}
			answer_FG_back_old = answer_FG_back;
			answer_FG_green_old = answer_FG_green;
			answer_FG_red_old = answer_FG_red;
			answer_KFZ_green_old = answer_KFZ_green;
			answer_KFZ_red_old = answer_KFZ_red;
			answer_KFZ_yellow_old = answer_KFZ_yellow;
			
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException ie) {
				Thread.currentThread().interrupt();
			}
		}
		
	}
}
