package iat;

import org.apache.plc4x.java.PlcDriverManager;
import org.apache.plc4x.java.api.PlcConnection;
import org.apache.plc4x.java.api.messages.PlcReadRequest;
import org.apache.plc4x.java.api.messages.PlcReadResponse;
import org.apache.plc4x.java.api.messages.PlcWriteRequest;
import org.apache.plc4x.java.api.messages.PlcWriteResponse;
import org.apache.plc4x.java.api.types.PlcResponseCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// Oberklasse
public class PLC4XHandler {
	
	// Init Logger, Drivermanager, Connection Object, Response Object, 
	public static final Logger logger = LoggerFactory.getLogger(PLC4XHandler.class);
	public PlcDriverManager DriverManager;
	public PlcConnection Connection;
	public PlcReadResponse Response;
	public PlcWriteResponse WriteResponse;

	// Input connectionString
	// enables connection between Server & PLC
	public PLC4XHandler(String connectionString) {
		this.DriverManager = new PlcDriverManager();
		connect(connectionString);
	}

	public void connect(String connectionString) {
		try {
			this.Connection = DriverManager.getConnection(connectionString);
		} catch (Exception e) {
			logger.error("Keine Verbindung möglich", e);
		}
	}

	// Input: identifierString
	// Output: returns value from requested variable
	public int read(String identifierString) {
		this.Response = readVariable(identifierString);
		System.out.println("Reading finished");
		String answer = printResponse(this.Response);
		if ((answer == "false")) {
			return 0;
		} else if (answer == "true") {
			return 1;
		}else {
			return Integer.parseInt(answer);	
		}
		
	}

	public PlcReadResponse readVariable(String identifier) {
		logger.info("Verbindung aufgebaut");
		if (this.Connection.getMetadata().canRead()) {
			PlcReadRequest.Builder builder = this.Connection.readRequestBuilder();

			builder.addItem("value", identifier);

			PlcReadRequest readRequest = builder.build();

			try {
				System.out.println("Read data ...");
				PlcReadResponse response = readRequest.execute().get();
				return response;

			} catch (Exception e) {
				System.out.println("Reading failed");
				return null;
			}

		} else {
			logger.info("Reading not possible");
			return null;
		}
	}

	// Input: identifierString
	// Output: no return
	public void write(String identifierString, boolean value) {

		this.WriteResponse = writeVariable(identifierString, value);
		printWriteResponse(this.WriteResponse);
		System.out.println("Writing finished");
	}

	public PlcWriteResponse writeVariable(String identifier, boolean value) {

		if (this.Connection.getMetadata().canWrite()) {
			System.out.println("Write data ...");
			PlcWriteRequest.Builder wbuilder = Connection.writeRequestBuilder();
			
			
			wbuilder.addItem("Integer", identifier, value);
			PlcWriteRequest writeRequest = wbuilder.build();

			try {
				PlcWriteResponse response = writeRequest.execute().get();
				return response;
			} catch (Exception e) {
				System.out.println("Writing failed");
				return null;
			}

		} else {
			logger.info("Reading not possible");
			return null;
		}

	}

	// Input: PlcReadResponse response
	// Output: returns String with value of requested variable
	public String printResponse(PlcReadResponse response) {
		String result = "";
		for (String fieldName : response.getFieldNames()) {
			if (response.getResponseCode(fieldName) == PlcResponseCode.OK) {

				int numValues = response.getNumberOfValues(fieldName);
				// If it's just one element, output just one single line.
				if (numValues == 1) {
					logger.info("Value[" + fieldName + "]: " + response.getObject(fieldName));
					result = String.valueOf(response.getObject(fieldName));

				}
				// If it's more than one element, output each in a single row.
				else {
					logger.info("Value[" + fieldName + "]:");
					for (int i = 0; i < numValues; i++) {
						logger.info(" - " + response.getObject(fieldName, i));
					}
				}
			}
			// Something went wrong, to output an error message instead.
			else {
				logger.error("Error[" + fieldName + "]: " + response.getResponseCode(fieldName).name());
			}
		}
		return result;

	}

	public void printWriteResponse(PlcWriteResponse writeresponse) {

		for (String fieldName : writeresponse.getFieldNames()) {
			if (writeresponse.getResponseCode(fieldName) == PlcResponseCode.OK) {
				logger.info("Value[" + fieldName + "]: updated");
			}
			// Something went wrong, to output an error message instead.
			else {
				logger.error("Error[" + fieldName + "]: " + writeresponse.getResponseCode(fieldName).name());
			}
		}
	}

	// Output: closes connection with PLC
	public void close() {
		try {
			this.Connection.close();
			System.out.println("Verbindung geschlossen");
		} catch (Exception e) {
			logger.error("Keine Verbindung", e);
		}
	}
}
